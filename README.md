
# TFF - TUI Use Case 

## Usage 
To get the project running on a local machine just run:


`npm install`


`npm start`


The install script will run both the json server and the react server.


After running install you should be able to access the application @ http://localhost:3000/


## Overview 
This is a small project developed using React, Typescript, Material UI, Jest and Json-Server so simulate a back-end API style connection.

You can watch a quick overview of the project running on [youtube.](https://www.youtube.com/watch?v=dmeSBH3IP1k)

In this project the objective was to allow an end user to edit the passengers information both a specific flight booking. The user can change all the fields of the passengers, however the each name field can only change up to 3 chars and the information can only be edited one time. Furthermore there must be a second step confirmation on these changes.

The passengers information is being load from the file `src/data/db.json`
The information is being fetched and persisted in this file using json-server.
If you wish to change the initial data alter this file and use the formar:

 ```json
{
  "passengers": [
    {
      "id": 0,
      "title": "MR",
      "gender": "FEMALE",
      "firstName": "Joy",
      "lastName": "Doe",
      "dateOfBirth": "2003-08-18"
    },
    {
      "id": 1,
      "title": "MR",
      "gender": "FEMALE",
      "firstName": "Jane",
      "lastName": "Done",
      "dateOfBirth": "2001-04-24"
    },
    ...
  ]
}
````