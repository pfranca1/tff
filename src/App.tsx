import * as React from 'react';
import HeaderBar from './components/HeaderBar/HeaderBar';
import { createTheme, makeStyles, ThemeProvider } from '@mui/material';
import BookingManager from './components/BookingManager/BookingManager';
import { Box } from '@mui/material';

const theme = createTheme({
  palette: {
    primary: {
      light: '#C2E6FA',
      main: '#70CBF4',
      dark: '#092A5E',
    }
  },
})

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Box bgcolor="grey.100">
        <HeaderBar />
        <BookingManager />
      </Box>
    </ThemeProvider>
  );
}

export default App;