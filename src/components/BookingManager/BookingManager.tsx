import * as React from 'react';
import { Container } from '@mui/material';
import BookingDetails from './components/BookingDetails/BookingDetails';
import FlightInformation from './components/FlightInformation/FlightInformation';
import PassengerDetails from './components/PassengerDetails/PassengerDetails';
import ContactDetails from './components/ContactDetails/ContactDetails';
import PricingDetails from './components/PricingDetails/PricingDetails';

function BookingManager() {
  return (
    <>
      <BookingDetails />
      <Container   
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          width: '60%',
          marginTop: '215px',
          '@media (max-width: 900px)': {
            width: '95%',
          }
        }}
      >
        <FlightInformation />
        <ContactDetails />
        <PassengerDetails />
        <PricingDetails />
      </Container>
    </>
  );
}

export default BookingManager;
