import React from 'react';
import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import axios from 'axios';
import PassengerDetails from './PassengerDetails';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('PassengerDetails Component', () => {
  const initialData = [
    { id: 1, gender: 'MALE', title: 'MR', firstName: 'John', lastName: 'Doe', dateOfBirth: '1990-01-01' }
  ];

  beforeEach(() => {
    mockedAxios.get.mockResolvedValue({ data: initialData });
    mockedAxios.put.mockResolvedValue({});
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('renders without crashing', () => {
    render(<PassengerDetails />);
    expect(screen.getByText('Passenger Details')).toBeInTheDocument();
  });

  it('fetches passengers on mount and updates state', async () => {
    render(<PassengerDetails />);
    await waitFor(() => {
      expect(mockedAxios.get).toHaveBeenCalled();
      expect(screen.getByDisplayValue('John')).toBeInTheDocument();
    });
  });

//   it('handles input changes correctly', async () => {
//     render(<PassengerDetails />);
//     await waitFor(() => {
//       const firstNameInput = screen.getByLabelText('First Name');
//       userEvent.clear(firstNameInput);
//       userEvent.type(firstNameInput, 'Joey');
//       expect(firstNameInput).toHaveValue('Joey');
//     });
//   });

  it('prevents character changes beyond limit for first name', async () => {
    render(<PassengerDetails />);
    await waitFor(() => {
      const firstNameInput = screen.getByLabelText('First Name');
      userEvent.clear(firstNameInput);
      userEvent.type(firstNameInput, 'Jonathan');
      expect(firstNameInput).not.toHaveValue('Jonathan'); 
    });
  });

  it('opens dialog on "Next" button click and displays changes summary', async () => {
    render(<PassengerDetails />);
    await waitFor(() => {
      const nextButton = screen.getByText('Next');
      userEvent.click(nextButton);
      expect(screen.getByText('Confirm Changes')).toBeInTheDocument();
    });
  });

//   it('saves changes when "Confirm" button is clicked', async () => {
//     render(<PassengerDetails />);
//     await waitFor(() => {
//       const nextButton = screen.getByText('Next');
//       userEvent.click(nextButton);
//     });
//     userEvent.click(screen.getByText('Confirm'));
//     await waitFor(() => {
//       expect(mockedAxios.put).toHaveBeenCalledTimes(initialData.length);
//       expect(screen.queryByText('Confirm Changes')).toBeNull();
//     });
//   });

//   it('handles API errors during save operations', async () => {
//     mockedAxios.put.mockRejectedValue(new Error('Failed to save'));
//     render(<PassengerDetails />);
//     await waitFor(() => {
//       const nextButton = screen.getByText('Next');
//       userEvent.click(nextButton);
//     });
//     userEvent.click(screen.getByText('Confirm'));
//     await waitFor(() => {
//       expect(screen.getByText('Failed to save some changes.')).toBeInTheDocument();
//     });
//   });
});

