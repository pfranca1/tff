import React, { useState, useEffect } from 'react';
import {
  Container, Accordion, AccordionSummary, AccordionDetails, Typography,
  FormControl, InputLabel, Select, MenuItem, TextField, Box, Button, Dialog,
  DialogActions, DialogContent, DialogContentText, DialogTitle,
  Avatar, useMediaQuery
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import axios from 'axios';
import { styled } from '@mui/system';
import GroupsOutlinedIcon from '@mui/icons-material/GroupsOutlined';

type Passenger = {
  id: number;
  gender: string;
  title: string;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
};

const IconContainer = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
}));

const PassengersDetails = () => {
  const [passengers, setPassengers] = useState<Passenger[]>([]);
  const [originalPassengers, setOriginalPassengers] = useState<Passenger[]>([]);
  const [open, setOpen] = useState(false);
  const [isChanged, setIsChanged] = useState(false); 

  const isMobile = useMediaQuery('(max-width:600px)');

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('http://localhost:3333/passengers');
        setPassengers(response.data);
        setOriginalPassengers(JSON.parse(JSON.stringify(response.data))); 
      } catch (error) {
        console.error('Error fetching data: ', error);
      }
    };

    fetchData();
  }, []);

  const handleInputChange = (index: number, field: keyof Passenger, value: string) => {
    if (isChanged) return; 

    const original = originalPassengers[index];
    if (field === 'firstName' || field === 'lastName') {
      
      if (
        Math.abs(value.length - original[field].length) > 3 ||
        [...value].filter((char, idx) => char !== original[field][idx]).length > 3
      ) {
        alert('You can only change up to 3 characters for ' + field);
        return; 
      }
    }

    const updatedPassengers = passengers.map((passenger, i) =>
      i === index ? { ...passenger, [field]: value } : passenger
    );
    setPassengers(updatedPassengers);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleConfirm = async () => {
    const updatePromises = passengers.map((passenger) =>
      axios.put(`http://localhost:3333/passengers/${passenger.id}`, passenger)
    );

    try {
      await Promise.all(updatePromises);
      alert('All changes have been saved successfully!');
      setIsChanged(true); 
      handleClose();
    } catch (error) {
      console.error('Error saving changes:', error);
      alert('Failed to save some changes.');
      handleClose();
    }
  };

  const renderChangeSummary = (): string[] => {
    const changes: string[] = [];
    passengers.forEach((passenger, index) => {
      const original = originalPassengers[index];
      if (!original) return;

      (Object.keys(passenger) as (keyof Passenger)[]).forEach((key) => {
        if (passenger[key] !== original[key]) {
          changes.push(
            `Passenger ID ${passenger.id} ${key} changed from '${original[key]}' to '${passenger[key]}'`
          );
        }
      });
    });
    return changes;
  };

  return (
    <Container sx={{ mt: 2 }}>
      <Accordion
        defaultExpanded
        sx={{ border: '3px solid', borderColor: 'primary.dark' }}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1-content"
          id="panel1-header"
        >
          <IconContainer>
            <Avatar sx={{ bgcolor: 'primary.light', marginRight: 1 }}>
              <GroupsOutlinedIcon sx={{ color: 'primary.dark' }} />
            </Avatar>
            <Typography sx={{ fontWeight: 'bold', color: 'primary.dark' }}>
              PASSENGER DETAILS
            </Typography>
            <Typography
              variant="body2"
              sx={{ color: 'primary.dark', marginLeft: '8px' }}
            >{`${passengers.length} Adult   0 Child   0 Infant`}</Typography>
          </IconContainer>
        </AccordionSummary>
        <AccordionDetails sx={{ bgcolor: 'primary.light' }}>
          {passengers.map((passenger, index) => (
            <Container
              key={passenger.id}
              sx={{
                bgcolor: 'white',
                p: 2,
                borderRadius: 2,
                mb: 2,
                ...(isMobile && { marginBottom: '16px' }) 
              }}
            >
              <Typography variant="subtitle1">
                Passenger Information
              </Typography>
              <form>
                <FormControl fullWidth margin="normal">
                  <InputLabel>Gender</InputLabel>
                  <Select
                    value={passenger.gender}
                    onChange={(e) =>
                      handleInputChange(index, 'gender', e.target.value as string)
                    }
                    name="gender"
                    disabled={isChanged}
                  >
                    <MenuItem value="MALE">Male</MenuItem>
                    <MenuItem value="FEMALE">Female</MenuItem>
                    <MenuItem value="OTHER">Other</MenuItem>
                  </Select>
                </FormControl>
                {(isMobile ? (
                  <>
                    <FormControl fullWidth sx={{ mt: 2 }}>
                      <InputLabel>Title</InputLabel>
                      <Select
                        value={passenger.title}
                        onChange={(e) =>
                          handleInputChange(index, 'title', e.target.value as string)
                        }
                        name="title"
                        disabled={isChanged}
                      >
                        <MenuItem value="MR">Mr.</MenuItem>
                        <MenuItem value="MS">Ms.</MenuItem>
                        <MenuItem value="MRS">Mrs.</MenuItem>
                      </Select>
                    </FormControl>
                    <FormControl fullWidth sx={{ mt: 2 }}>
                      <TextField
                        label="First Name"
                        fullWidth
                        value={passenger.firstName}
                        onChange={(e) =>
                          handleInputChange(index, 'firstName', e.target.value)
                        }
                        disabled={isChanged}
                      />
                    </FormControl>
                    <FormControl fullWidth sx={{ mt: 2 }}>
                      <TextField
                        label="Last Name"
                        fullWidth
                        value={passenger.lastName}
                        onChange={(e) =>
                          handleInputChange(index, 'lastName', e.target.value)
                        }
                        disabled={isChanged}
                      />
                    </FormControl>
                  </>
                ) : (
                  <Box sx={{ display: 'flex', gap: 2 }}>
                    <FormControl fullWidth>
                      <InputLabel>Title</InputLabel>
                      <Select
                        value={passenger.title}
                        onChange={(e) =>
                          handleInputChange(index, 'title', e.target.value as string)
                        }
                        name="title"
                        disabled={isChanged}
                      >
                        <MenuItem value="MR">Mr.</MenuItem>
                        <MenuItem value="MS">Ms.</MenuItem>
                        <MenuItem value="MRS">Mrs.</MenuItem>
                      </Select>
                    </FormControl>
                    <FormControl fullWidth>
                      <TextField
                        label="First Name"
                        fullWidth
                        value={passenger.firstName}
                        onChange={(e) =>
                          handleInputChange(index, 'firstName', e.target.value)
                        }
                        disabled={isChanged}
                      />
                    </FormControl>
                    <FormControl fullWidth>
                      <TextField
                        label="Last Name"
                        fullWidth
                        value={passenger.lastName}
                        onChange={(e) =>
                          handleInputChange(index, 'lastName', e.target.value)
                        }
                        disabled={isChanged}
                      />
                    </FormControl>
                  </Box>
                ))}
                <TextField
                  label="Date of Birth"
                  fullWidth
                  type="date"
                  value={passenger.dateOfBirth}
                  onChange={(e) =>
                    handleInputChange(index, 'dateOfBirth', e.target.value)
                  }
                  InputLabelProps={{ shrink: true }}
                  disabled={isChanged}
                  sx={{ mt: 2 }}
                />
              </form>
            </Container>
          ))}
          <Box sx={{ display: 'flex', justifyContent: 'space-between', mt: 2 }}>
            <Button variant="contained" sx={{ bgcolor: 'primary.dark', color: 'white' }}>
              Back
            </Button>
            <Button
              variant="contained"
              onClick={handleClickOpen}
              sx={{ bgcolor: 'primary.dark', color: 'white' }}
              disabled={isChanged}
            >
              Next
            </Button>
          </Box>
        </AccordionDetails>
      </Accordion>

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Confirm Changes"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to save these changes?
            {renderChangeSummary().length > 0 ? (
              <ul>
                {renderChangeSummary().map((change, index) => (
                  <li key={index}>{change}</li>
                ))}
              </ul>
            ) : (
              <p>No changes made.</p>
            )}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleConfirm} autoFocus>
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </Container>
  );
};

export default PassengersDetails;
