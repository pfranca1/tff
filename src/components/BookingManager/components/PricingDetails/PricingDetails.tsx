import * as React from 'react';
import { Accordion, AccordionDetails, AccordionSummary, Container, Typography, Avatar } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { styled } from '@mui/system';
import LocalOfferOutlinedIcon from '@mui/icons-material/LocalOfferOutlined';

const IconContainer = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
}));

function PricingDetails() {
  return (
    <Container sx={{ mt: 2 }}>
      <Accordion sx={{ border: '3px solid', borderColor: 'primary.dark' }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2-content"
          id="panel2-header"
        >
          <IconContainer>
            <Avatar sx={{ bgcolor: 'primary.light', marginRight: 1 }}>
              <LocalOfferOutlinedIcon sx={{ color: 'primary.dark' }} />
            </Avatar>
            <Typography sx={{ fontWeight: 'bold', color: 'primary.dark' }}>PRICING DETAILS</Typography>
          </IconContainer>
        </AccordionSummary>
        <AccordionDetails>

        </AccordionDetails>
      </Accordion>
    </Container>
  );
}

export default PricingDetails;
