import * as React from 'react';
import { Box, Container, Divider, Stack, Typography } from '@mui/material';
import FlightTakeoffIcon from '@mui/icons-material/FlightTakeoff';
import FlightLandIcon from '@mui/icons-material/FlightLand';
import FlightClassIcon from '@mui/icons-material/FlightClass';

function FlightInformation() {
  return (

    <Container>
     <Stack direction="column" sx={{ mt: 2, height: '170px', border: '3px solid', borderColor: 'primary.dark', borderRadius: '5px' }}>
      <Box sx={{ bgcolor: 'white', flex: '1 1 auto', display: 'flex', alignItems: 'center', padding: '16px' }}>
        <FlightTakeoffIcon sx={{ color: 'primary.dark' }} />
        <Typography variant="body1" sx={{ marginLeft: '8px' }}>HAJ</Typography>
        <Typography variant="body2" sx={{ marginLeft: '8px' }}>26.10.2023 - 14:15</Typography>
        <Divider orientation="horizontal" flexItem sx={{ marginLeft: 'auto', marginRight: '16px' }} />
        <FlightLandIcon sx={{ color: 'primary.dark' }} />
        <Typography variant="body1" sx={{ marginLeft: '8px' }}>PMI</Typography>
        <Typography variant="body2" sx={{ marginLeft: '8px' }}>26.10.2023 - 26:50</Typography>
      </Box>
        <Box sx={{ bgcolor: 'primary.light', height: '30px', display: 'flex', alignItems: 'center', paddingLeft: '8px' }}>
          <FlightClassIcon sx={{ color: 'primary.dark' }} />
          <Typography variant="body1" sx={{ fontWeight: 'bold', color: 'primary.dark', marginLeft: '8px' }}>
            SEAT RESERVATION
          </Typography>
          <Typography variant="body1" sx={{ fontWeight: 'bold', color: 'primary.dark', marginLeft: 'auto' }}>
            PRICE
          </Typography>
          <Typography variant="body1" sx={{ color: 'primary.dark', marginLeft: '8px', marginRight: '8px' }}>
            €114.99
          </Typography>
        </Box>
      </Stack>
      <Stack direction="column" sx={{ mt: 2, height: '170px', border: '3px solid', borderColor: 'primary.dark', borderRadius: '5px' }}>
      <Box sx={{ bgcolor: 'white', flex: '1 1 auto', display: 'flex', alignItems: 'center', padding: '16px' }}>
        <FlightTakeoffIcon sx={{ color: 'primary.dark' }} />
        <Typography variant="body1" sx={{ marginLeft: '8px' }}>PMI</Typography>
        <Typography variant="body2" sx={{ marginLeft: '8px' }}>29.10.2023 - 20:10</Typography>
        <Divider orientation="horizontal" flexItem sx={{ marginLeft: 'auto', marginRight: '16px' }} />
        <FlightLandIcon sx={{ color: 'primary.dark' }} />
        <Typography variant="body1" sx={{ marginLeft: '8px' }}>HAJ</Typography>
        <Typography variant="body2" sx={{ marginLeft: '8px' }}>29.10.2023 - 22:50</Typography>
      </Box>
        <Box sx={{ bgcolor: 'primary.light', height: '30px', display: 'flex', alignItems: 'center', paddingLeft: '8px' }}>
          <FlightClassIcon sx={{ color: 'primary.dark' }} />
          <Typography variant="body1" sx={{ fontWeight: 'bold', color: 'primary.dark', marginLeft: '8px' }}>
            SEAT RESERVATION
          </Typography>
          <Typography variant="body1" sx={{ fontWeight: 'bold', color: 'primary.dark', marginLeft: 'auto' }}>
            PRICE
          </Typography>
          <Typography variant="body1" sx={{ color: 'primary.dark', marginLeft: '8px', marginRight: '8px' }}>
            €313.99
          </Typography>
        </Box>
      </Stack>
    </Container>

  );
}

export default FlightInformation;