import * as React from 'react';
import { Box, Button, Chip, Container, Typography } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import AirplaneTicketIcon from '@mui/icons-material/AirplaneTicket';
import SettingsBackupRestoreIcon from '@mui/icons-material/SettingsBackupRestore';
import CommentIcon from '@mui/icons-material/Comment';
import InfoIcon from '@mui/icons-material/Info';
import CachedIcon from '@mui/icons-material/Cached';

function BookingDetails() {
  const [isScrolled, setIsScrolled] = React.useState(false);
  const theme = useTheme();

  React.useEffect(() => {
    const handleScroll = () => {
      const position = window.pageYOffset;
      setIsScrolled(position > 100);
    };

    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
  return (
    <>
      {!isScrolled && (
        <>
          <Box
            sx={{
              height: 150,
              bgcolor: 'primary.dark',
              position: 'fixed',
              width: '100%',
              zIndex: 1000,
              top: 64
            }}
          >
            <Container maxWidth="xl" sx={{ backgroundColor: "primary", display: 'flex', flexDirection: 'column', marginLeft: '48px', marginRight: '48px' }}>
              <Box sx={{ flex: '1 3 auto', display: 'flex', alignItems: 'center', padding: '16px' }}>
                <AirplaneTicketIcon sx={{ color: 'primary.light', marginLeft: '180px' }} />
                <Typography variant="h6" sx={{ marginLeft: '16px', color: 'white' }}>BOOKING DETAILS</Typography>
                <SettingsBackupRestoreIcon sx={{ color: 'primary.light', marginLeft: '16px' }} />
                <CommentIcon sx={{ color: 'primary.light', marginLeft: '16px' }} />
                <InfoIcon sx={{ color: 'primary.light', marginLeft: '16px' }} />
                <CachedIcon sx={{ color: 'primary.light', marginLeft: '16px' }} />
                <Chip label="Confirmed" color="success" sx={{ marginLeft: '76px' }} />
                <Chip label="TOTAL in EUR 428,98" color="primary" sx={{ marginLeft: 'auto', color: "white", marginRight: '260px' }} />
              </Box>
              <Box sx={{ flex: '2 3 auto', display: 'flex', alignItems: 'center' }}>
                <Typography variant="h4" sx={{ marginLeft: '200px', color: 'white' }}> ZY1ZYT</Typography>
                <Typography variant="h4" sx={{ marginLeft: 'auto', color: "white", marginRight: '275px' }}>04.09.2023  16:26</Typography>
              </Box>
            </Container>
          </Box>

          <Box
            sx={{
              height: 50,
              bgcolor: 'primary.light',
              display: 'flex',
              justifyContent: 'center',
              p: 1,
              position: 'fixed',
              top: '214px', 
              width: '100%',
              zIndex: 1000,
            }}
          >
            <Container sx={{
              display: 'flex',
              justifyContent: 'center',
              gap: 2,
              width: '80%'
            }}>
              <Button variant="contained" sx={{ bgcolor: 'primary.dark', color: "white", width: '170px' }}>REMARKS</Button>
              <Button variant="contained" sx={{ bgcolor: 'primary.dark', color: "white", width: '170px' }}>REBOOK</Button>
              <Button variant="contained" sx={{ bgcolor: 'primary.dark', color: "white", width: '170px' }}>CANCEL</Button>
              <Button variant="contained" sx={{ bgcolor: 'primary.dark', color: "white", width: '170px' }}>HISTORY</Button>
              <Button variant="contained" sx={{ bgcolor: 'grey', color: "white", width: '170px' }}>FLIGHT CHANGES</Button>
            </Container>
          </Box>
        </>)}
      {isScrolled && (
        <Box
          sx={{
            height: 50,
            bgcolor: 'primary.dark',
            display: 'flex',
            justifyContent: 'flex-end',
            p: 1,
            position: 'fixed',
            top: 64,
            width: '100%',
            zIndex: 1000,
          }}
        >
          <Container sx={{
            display: 'flex',
            justifyContent: 'left',
            alignItems: 'center',
            gap: 5,
            height: 50,
          }}>
            <Typography variant="h6" sx={{ color: 'white' }}>ZY1ZYT</Typography>
            <Typography variant="h6" sx={{ color: 'white' }}>04.09.2023 16:26</Typography>
            <Typography variant="h6" sx={{ color: 'white' }}>Confirmed</Typography>
            <Typography variant="h6" sx={{ color: 'white' }}>428,98</Typography>
          </Container>
          <Container sx={{
            display: 'flex',
            justifyContent: 'right',
            gap: 1,
            height: 50,
            alignItems: 'center',
          }}>
            {/* Buttons */}
            <Button variant="contained" sx={{ bgcolor: 'primary.light', color: "primary.dark" }}>REMARKS</Button>
            <Button variant="contained" sx={{ bgcolor: 'primary.light', color: "primary.dark" }}>REBOOK</Button>
            <Button variant="contained" sx={{ bgcolor: 'primary.light', color: "primary.dark" }}>CANCEL</Button>
            <Button variant="contained" sx={{ bgcolor: 'primary.light', color: "primary.dark" }}>HISTORY</Button>
            <Button variant="contained" sx={{ bgcolor: 'grey', color: "primary.dark" }}>FLIGHT CHANGES</Button>
          </Container>
        </Box>
      )
      }

    </>
  );
}

export default BookingDetails;
